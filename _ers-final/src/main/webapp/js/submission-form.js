function showRequestForm() {
	let form = document.getElementById('reimb-form');
	
	if(form.classList.contains('ers-hidden')) {
		form.classList.remove('ers-hidden');
	} else {
		form.classList.add('ers-hidden');
	}
}

function submitRequest() {
    
	let amountBox = document.getElementById('reimb-amount');
	let descriptionBox = document.getElementById('reimb-description');
	let success = document.getElementById('form-success');
	
	amountBox.classList.remove('ers-invalid');
	descriptionBox.classList.remove('ers-invalid');
	success.classList.add('ers-hidden');
	
    let inAmount = amountBox.value;
    let inDescription = descriptionBox.value;
    let typeSelect = document.getElementById('reimb-type');
    let inType = typeSelect.options[typeSelect.selectedIndex].value;
    let request = { "amount":inAmount, "description":inDescription, "typeId":inType };
    
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = () => {
        
        if(xhttp.readyState === 4) {
        	document.getElementById('loading-screen').classList.add('ers-hidden');
            if(xhttp.status === 200) {
            	success.classList.remove('ers-hidden');
            	success.innerText = 'SUCCESS';
            	amountBox.value = null;
            	descriptionBox.value = null;
            	result = JSON.parse(xhttp.responseText);
            	buildRequestTable(result);
            } else {
            	success.classList.remove('ers-hidden');
            	success.innerText = 'FAILED';
            	amountBox.classList.add('ers-invalid');
            	descriptionBox.classList.add('ers-invalid');
            }
        }
    }
    
    document.getElementById('loading-screen').classList.remove('ers-hidden');
    xhttp.open('POST', './fetch');
    xhttp.send(JSON.stringify(request));
}
