// Purpose: Handles login requests to the server.

function loginjs()
{
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {

        if(xhttp.readyState === 4)
        {
            if(xhttp.status === 200)
            {
                userLoggedIn(xhttp);
            }
            else
            {
                console.log('Data request failed.');
            }
        }
    }

    xhttp.open('GET', './login');
    xhttp.send();
}

function startLogin()
{
    let success = true;
    let form = document.getElementById('login-form');
    let userBox = document.getElementById('login-user');
    let passBox = document.getElementById('login-pass');
    let submitButton = document.getElementById('login-submit');

    success = formBoxIsEmpty(userBox) && success;
    success = formBoxIsEmpty(passBox) && success;

    if(success)
    {
        let xhttp = new XMLHttpRequest();
        xhttp.open('POST', './login');
        xhttp.onload = (resp) => {

            userLoggedIn(xhttp);
            document.getElementById('loading-screen').classList.add('ers-hidden');
        }

        let login = { "username":userBox.value, "password":passBox.value };
        xhttp.send(JSON.stringify(login));
        document.getElementById('loading-screen').classList.remove('ers-hidden');

        userBox.setAttribute('disabled', 'disabled');
        passBox.setAttribute('disabled', 'disabled');
        submitButton.setAttribute('disabled', 'disabled');
    }
}

function formBoxIsEmpty(object)
{
    if(!object.value)
    {
        object.classList.add('ers-invalid');
        return false;
    }
    else
    {
        object.classList.remove('ers-invalid');
        return true;
    }
}

function userLoggedIn(resp)
{
    let user = JSON.parse(resp.responseText);

    if(user)
    {
        console.log('user logged in');
        let form = document.getElementById('login-form');
        let userBox = document.getElementById('login-user');
        let passBox = document.getElementById('login-pass');
        let submitButton = document.getElementById('login-submit');
        let userButton = document.getElementById('user-buttons');
        let logoutSpan = document.getElementById('logout-span');
        
        logoutSpan.innerHTML = '<a href="./logout" class="ers-padding-left-6 nav-item nav-link">(Log Out)</a>';

        userBox.remove();
        passBox.remove();
        submitButton.remove();

        let text = document.createElement('span');
        text.innerText = `Welcome, ${user.firstName} `;
        text.classList.add('whiteText');

        form.appendChild(text);
        
        let newRequest = document.createElement('li');
        newRequest.classList.add('nav-item');
        
        let reqFormLink = document.createElement('a');
        reqFormLink.classList.add('nav-link');
        reqFormLink.onclick = showRequestForm;
        reqFormLink.innerText = 'New Request';
        reqFormLink.href = '#';
        
        newRequest.appendChild(reqFormLink);
        userButton.appendChild(newRequest);
        
        newRequest = document.createElement('li');
        newRequest.classList.add('nav-item');
        
        reqFormLink = document.createElement('a');
        reqFormLink.classList.add('nav-link');
        reqFormLink.innerText = 'My Requests';
        reqFormLink.href = './home';
        
        newRequest.appendChild(reqFormLink);
        userButton.appendChild(newRequest);
        
        if(user.roleID === 2) {
        	
            
            newRequest = document.createElement('li');
            newRequest.classList.add('nav-item');
            
            reqFormLink = document.createElement('a');
            reqFormLink.classList.add('nav-link');
            reqFormLink.onclick = managerFetch;
            reqFormLink.innerText = 'All Requests';
            reqFormLink.href = '#';
            
            newRequest.appendChild(reqFormLink);
            userButton.appendChild(newRequest);
        }

        fetchRequests();
    }
    else
    {
        console.log('login failed');
        let userBox = document.getElementById('login-user');
        let passBox = document.getElementById('login-pass');
        let submitButton = document.getElementById('login-submit');

        userBox.removeAttribute('disabled');
        passBox.removeAttribute('disabled');
        submitButton.removeAttribute('disabled');

        console.log('no user logged in');
    }
}

function showRequestForm() {
	let form = document.getElementById('reimb-form');
	
	if(form.classList.contains('ers-hidden')) {
		form.classList.remove('ers-hidden');
	} else {
		form.classList.add('ers-hidden');
	}
	
}