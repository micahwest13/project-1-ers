
function managerFetch() {
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {
        
        if(xhttp.readyState === 4) {
        	document.getElementById('loading-screen').classList.add('ers-hidden');
            if(xhttp.status === 200) {
                let result = JSON.parse(xhttp.responseText);
                
                buildManagerTable(result);
            } else {
            	
            }
        }
    }
    
    xhttp.open('GET', './managerfetch');
    xhttp.send();
    document.getElementById('loading-screen').classList.remove('ers-hidden');
    
    let tableContainer = document.getElementById('table-container');
    tableContainer.innerHTML = '';
    
    
}

let tableFormat = [
	[ 'Type', 'Status', 'Amount', 'Author', 'Description', 'Date Submitted', 'Resolver', 'Date Resolved' ], // Table Header Fields
	[ 'type', 'status', 'amount', 'author', 'description', 'submissionDate', 'resolver', 'resolutionDate' ], // Data Field Names
	[ '', '', 'ers-right-align', '', '', '', '', '' ] // Style Classes
];

function buildManagerTable(result) {
	
	let tableContainer = document.getElementById('table-container');
	tableContainer.innerHTML = '';
	
	result = formatTickets(result);
	
	let pendingTickets = result.filter( (ticket) => {
		return ticket.status === 'Pending';
	});
	
	let approvedTickets = result.filter( (ticket) => {
		return ticket.status === 'Approved';
	});
	
	let deniedTickets = result.filter( (ticket) => {
		return ticket.status === 'Denied';
	});
	
	let headPending = document.createElement('h2');
	headPending.innerText = 'Pending Tickets:';
	headPending.classList.add('ers-padding-12');
	tableContainer.appendChild(headPending);
	
	let buttonDiv = document.createElement('div');
	
	let approveButton = document.createElement('button');
	approveButton.classList.add('btn', 'btn-success');
	approveButton.setAttribute('disabled', 'disabled');
	approveButton.id = 'btn-approve';
	approveButton.innerText = 'Approve';
	approveButton.onclick = approveRequests;
	
	let denyButton = document.createElement('button');
	denyButton.classList.add('btn', 'btn-danger');
	denyButton.setAttribute('disabled', 'disabled');
	denyButton.id = 'btn-deny';
	denyButton.innerText = 'Deny';
	denyButton.onclick = denyRequests;
	
	buttonDiv.appendChild(approveButton);
	buttonDiv.appendChild(denyButton);
	
	tableContainer.appendChild(buttonDiv);
	
	drawTable(pendingTickets, true, 'pending-ticket-');
	
	let headPast = document.createElement('h2');
	headPast.innerText = 'Past Tickets:';
	headPast.classList.add('ers-padding-12');
	tableContainer.appendChild(headPast);
	
	let headApproved = document.createElement('h3');
	headApproved.innerText = 'Approved Tickets:';
	headApproved.classList.add('ers-padding-12');
	tableContainer.appendChild(headApproved);
	drawTable(approvedTickets, false, 'approved-ticket-');
	
	let headDenied = document.createElement('h3');
	headDenied.innerText = 'Denied Tickets:';
	headDenied.classList.add('ers-padding-12');
	tableContainer.appendChild(headDenied);
	drawTable(deniedTickets, false, 'denied-ticket-');
}

function formatTickets(result) {
	for(let i = 0; i < result.length; i++) {
		result[i].amount = '$ ' + result[i].amount.toFixed(2);
		
		if(result[i].submissionDate && result[i].submissionDate.includes('.')) {
			result[i].submissionDate = result[i].submissionDate.substring(0, result[i].submissionDate.indexOf('.'));
		}
		
		if(result[i].resolutionDate && result[i].resolutionDate.includes('.')) {
			result[i].resolutionDate = result[i].resolutionDate.substring(0, result[i].resolutionDate.indexOf('.'));
		}
		
		for(let j = 0; j < tableFormat[1].length; j++) {
			if(!result[i][tableFormat[1][j]]) {
				result[i][tableFormat[1][j]] = '--';
			}
		}
	}
	return result;
}

function drawTable(result, check, idPrefix) {
	
	let tableContainer = document.getElementById('table-container');
	let tableMain = document.createElement('table');
	tableMain.classList.add('table', 'table-hover');
	
	let tableHead = document.createElement('thead');
	let tableHeadings = document.createElement('tr');
	let tableHeadData;
	
	if(check) {
		let checkBox = document.createElement('input');
		tableHeadData = document.createElement('th');
		checkBox.type = 'checkbox';
		checkBox.classList.add('form-check-input', 'ers-checkbox');
		checkBox.id = idPrefix + 'check-master';
		checkBox.onclick = () => {
			checkTicketsById(idPrefix,checkBox.checked);
			changeApprovalButtons(idPrefix);
		};
		tableHeadData.appendChild(checkBox);
		tableHeadings.appendChild(tableHeadData);
	}
	
	for(let i = 0; i < tableFormat[0].length; i++) {
		tableHeadData = document.createElement('th');
		tableHeadData.innerHTML = tableFormat[0][i];
		tableHeadings.appendChild(tableHeadData);
	}
	
	tableHead.appendChild(tableHeadings);
	tableMain.appendChild(tableHead);
	
	let tableBody = document.createElement('tbody');
	
	for(let i = 0; i < result.length; i++) {
		
		let tableRow = document.createElement('tr');
		tableRow.id = idPrefix + i;
		tableRow.obj = result[i];
		let tableData;
		
		if(check) {
			let checkBox = document.createElement('input');
			tableData = document.createElement('td');
			checkBox.type = 'checkbox';
			checkBox.classList.add('form-check-input', 'ers-checkbox');
			checkBox.id = idPrefix + 'check-' + i;
			checkBox.onclick = () => {
				changeApprovalButtons(idPrefix);
			};
			tableData.appendChild(checkBox);
			tableRow.appendChild(tableData);
		}
		
		
		for(let j = 0; j < tableFormat[0].length; j++) {
			tableData = document.createElement('td');
			tableData.className = tableFormat[2][j];
			tableData.innerHTML = result[i][tableFormat[1][j]];
			tableRow.appendChild(tableData);
		}
		
		tableBody.appendChild(tableRow);
	}
	
	tableMain.appendChild(tableBody);
	tableContainer.appendChild(tableMain);
}

function checkTicketsById(prefix, checkTo) {
	
	let index = 0;
	let element = document.getElementById(prefix + index);
	
	while(element) {
		
		document.getElementById(prefix + 'check-' + index).checked = checkTo;
		
		index++;
		element = document.getElementById(prefix + index);
	}
}

function changeApprovalButtons(prefix) {
	
	let index = 0;
	let element = document.getElementById(prefix + index);
	let active = false;
	
	while(element && !active) {
		
		active = document.getElementById(prefix + 'check-' + index).checked;
		
		index++;
		element = document.getElementById(prefix + index);
	}
	
	if(active) {
		document.getElementById('btn-approve').removeAttribute('disabled');
		document.getElementById('btn-deny').removeAttribute('disabled');
	} else {
		document.getElementById('btn-approve').setAttribute('disabled', 'disabled');
		document.getElementById('btn-deny').setAttribute('disabled', 'disabled');
	}
}

function approveRequests() {
	let result = getCheckedRequests('pending-ticket-', 2); // 2 for approved.
	document.getElementById('loading-screen').classList.remove('ers-hidden');
	
	let json = JSON.stringify(result);
	
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = () => {
        
        if(xhttp.readyState === 4) {
            if(xhttp.status === 200) {
                let result = JSON.parse(xhttp.responseText);
                
                buildManagerTable(result);
                document.getElementById('loading-screen').classList.add('ers-hidden');
            } else {
            	
            }
        }
    }
	
	xhttp.open('PUT', './managerfetch');
    xhttp.send(json);
}

function denyRequests() {
	let result = getCheckedRequests('pending-ticket-', 3); // 3 for denied.
	document.getElementById('loading-screen').classList.remove('ers-hidden');
	
	let json = JSON.stringify(result);
	
	let xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = () => {
        
        if(xhttp.readyState === 4) {
            if(xhttp.status === 200) {
                let result = JSON.parse(xhttp.responseText);
                
                buildManagerTable(result);
                document.getElementById('loading-screen').classList.add('ers-hidden');
            } else {
            	
            }
        }
    }
	
	xhttp.open('PUT', './managerfetch');
    xhttp.send(json);
}

function getCheckedRequests(prefix, status) {
	
	let requests = [];
	
	let index = 0;
	let element = document.getElementById(prefix + index);
	
	while(element) {
		
		if(document.getElementById(prefix + 'check-' + index).checked) {
			let item = element.obj;
			let put = {id:item.id, statusId:status};
			
			requests.push(put);
		}
		
		index++;
		element = document.getElementById(prefix + index);
	}
	
	return requests;
}