package com.revature.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.beans.ClientTicket;
import com.revature.beans.ReimbTicket;
import com.revature.beans.User;
import com.revature.daos.RequestDAO;
import com.revature.daos.RequestStatusDAO;
import com.revature.daos.RequestTypeDAO;
import com.revature.daos.UserDAO;
import com.revature.daos.jdbc.RequestJDBC;
import com.revature.daos.jdbc.RequestStatusJDBC;
import com.revature.daos.jdbc.RequestTypeJDBC;
import com.revature.daos.jdbc.UserJDBC;
import com.revature.exceptions.ErsHttpException;
import com.revature.exceptions.ErsUnauthorizedException;

public class ErsRequestor {
	private RequestDAO daoReimb = new RequestJDBC();
	private UserDAO daoUser = new UserJDBC();
	private RequestStatusDAO daoStatus = new RequestStatusJDBC();
	private RequestTypeDAO daoType = new RequestTypeJDBC();
	private Logger log = Logger.getRootLogger();
	private JSONParser jsonp = new JSONParser();

	public boolean handleNewReimbursement(HttpServletRequest request) throws ErsHttpException {
		User currentUser = (User) request.getSession().getAttribute("user");

		if (currentUser == null) {
			throw new ErsUnauthorizedException();
		} else {
			try {
				int userID = currentUser.getUserId();
				ObjectMapper map = new ObjectMapper();
				String json = request.getReader().lines().reduce((acc, cur) -> acc + cur).get();
				log.debug("JSON parsed from client: " + json);

				ReimbTicket reimb = map.readValue(json, ReimbTicket.class);
				reimb.setAuthor(userID);
				
				if(reimb.getAmount() == 0 || "".equals(reimb.getDescription())) {
					return false;
				}

				log.info("Writing reimbursement object " + reimb + " to database.");

				int success = daoReimb.createRequest(reimb);

				if (success > 0) {
					log.info("Reimbursement request written to database. Updating user session...");
					
					List<ReimbTicket> newTickets = daoReimb.getRequestsByUserId(currentUser.getUserId());
					List<ClientTicket> outTickets = convertToClientTickets(newTickets);
					
					currentUser.setClientRequests(outTickets);
					
					
					request.getSession().setAttribute("user", currentUser);

					log.info("Session update complete. Returning to fetch controller.");
					return true;
					
				} else {
					return false;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean updateTicketStatus(HttpServletRequest request) throws ErsHttpException {
		User currentUser = (User) request.getSession().getAttribute("user");
		
		if (currentUser == null) {
			throw new ErsUnauthorizedException();
		} else {
			try {
				String json = request.getReader().lines().reduce((acc, cur) -> acc + cur).get();
				log.debug("JSON parsed from client: " + json);

				ReimbTicket reimbList[] = jsonp.JSONParse(json, ReimbTicket[].class);
				
				for(int i = 0; i < reimbList.length; i++) {
					
					if(reimbList[i].getAuthor() == currentUser.getUserId()) {
						log.warn("User attempted to approve own user request.");
						continue;
					}
					
					ReimbTicket reimb = reimbList[i];
					
					int id = reimb.getId();
					int userId = currentUser.getUserId();
					int status = reimb.getStatusId();
					
					log.debug("Updating Reimbursement Ticket in database.");
					daoReimb.updateStatus(id, userId, status);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}

	public ReimbTicket getRequestById(int id) {
		return daoReimb.getRequestsByUserId(id).get(0);
	}

	public List<ClientTicket> convertToClientTickets(List<ReimbTicket> list) {
		List<ClientTicket> newTickets = new ArrayList<ClientTicket>();

		Map<Integer, String> userMap = daoUser.getUserFullNames();
		Map<Integer, String> statusMap = new HashMap<Integer, String>();
		Map<Integer, String> typeMap = new HashMap<Integer, String>();

		for (int i = 0; i < list.size(); i++) {
			
			ClientTicket ticket = new ClientTicket(list.get(i));
			String author;
			String resolver;
			String status;
			String type;
			
			int userId = list.get(i).getAuthor();
			int resolverId = list.get(i).getResolver();
			int statusId = list.get(i).getStatusId();
			int typeId = list.get(i).getTypeId();
			
			if (!userMap.containsKey(userId)) {
				author = daoUser.getFullNameById(userId);
				userMap.put(userId, author);
			} else {
				author = userMap.get(userId);
			}
			
			if (!userMap.containsKey(resolverId)) {
				resolver = daoUser.getFullNameById(resolverId);
				userMap.put(resolverId, resolver);
			} else {
				resolver = userMap.get(resolverId);
			}
			
			if (!statusMap.containsKey(statusId)) {
				status = daoStatus.getStatusNameById(statusId);
				statusMap.put(statusId, status);
			} else {
				status = statusMap.get(statusId);
			}
			
			if (!typeMap.containsKey(typeId)) {
				type = daoType.getTypeNameById(typeId);
				typeMap.put(typeId, type);
			} else {
				type = typeMap.get(typeId);
			}
			
			ticket.setAuthor(author);
			ticket.setResolver(resolver);
			ticket.setStatus(status);
			ticket.setTypeId(type);
			
			newTickets.add(ticket);
		}

		return newTickets;
	}
}
