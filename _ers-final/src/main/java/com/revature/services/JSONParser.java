package com.revature.services;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class JSONParser {
	public static Logger log = Logger.getRootLogger();
	
	public String JSONStringify(Object obj) {
		try {
			ObjectMapper map = new ObjectMapper();
			ObjectWriter writer = map.writer().withDefaultPrettyPrinter();
			String json = writer.writeValueAsString(obj);
			return json;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T JSONParse(String json, Class dest) {
		log.trace("json received: " + json);
		T obj = null;
		ObjectMapper map = new ObjectMapper();
		try {
			obj = (T) map.readValue(json, dest);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return obj;
	}
}
