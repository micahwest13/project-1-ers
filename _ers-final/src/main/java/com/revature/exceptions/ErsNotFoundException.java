package com.revature.exceptions;

public class ErsNotFoundException extends ErsHttpException
{
	// [Eclipse] Generated Field
	private static final long serialVersionUID = 1864015486971739959L;

	public ErsNotFoundException()
	{
		super(404); // 404 Not Found
	}
}
