package com.revature.exceptions;

public class ErsUnauthorizedException extends ErsHttpException {
	private static final long serialVersionUID = 2027246452074571537L;

	public ErsUnauthorizedException() {
		super(401); // 401 Unauthorized
	}
}
