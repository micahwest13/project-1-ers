package com.revature.daos.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.revature.daos.RequestTypeDAO;
import com.revature.util.ConnectionUtil;

public class RequestTypeJDBC implements RequestTypeDAO {

	private static Logger log = Logger.getRootLogger();
	private static ConnectionUtil connUtil = ConnectionUtil.getConnectionUtil();

	@Override
	public String getTypeNameById(int id) {
		log.debug("Finding type of id " + id);

		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn
					.prepareStatement("SELECT reimb_type FROM ers_reimbursement_type WHERE reimb_type_id = ?");
			stmt.setInt(1, id);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return results.getString("reimb_type");
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire type from database.");
		}

		return null;
	}
}
