package com.revature.daos.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.revature.beans.ReimbTicket;
import com.revature.beans.User;
import com.revature.daos.RequestDAO;
import com.revature.daos.UserDAO;
import com.revature.util.ConnectionUtil;

public class UserJDBC implements UserDAO {

	private static Logger log = Logger.getRootLogger();
	private static ConnectionUtil connUtil = ConnectionUtil.getConnectionUtil();
	private static RequestDAO requestDAO = new RequestJDBC();

	private static String getFullNameFromResultSet(ResultSet result) throws SQLException {
		return result.getString("user_first_name") + " " + result.getString("user_last_name");
	}

	private static User getUserFromResultSet(ResultSet result) throws SQLException {
		User resultUser = new User();
		resultUser.setUserId(result.getInt("ers_users_id"));
		resultUser.setUserName(result.getString("ers_username"));
		resultUser.setUserPassword(result.getString("ers_password"));
		resultUser.setFirstName(result.getString("user_first_name"));
		resultUser.setLastName(result.getString("user_last_name"));
		resultUser.setEmail(result.getString("user_email"));
		resultUser.setRoleID(result.getInt("user_role_id"));
		resultUser.setStatusID(result.getInt("user_status_id"));

		List<ReimbTicket> requestList = new ArrayList<ReimbTicket>();
		requestList = requestDAO.getRequestsByUserId(resultUser.getUserId());
		resultUser.setRequests(requestList);

		return resultUser;
	}

	@Override
	public User getUserByUsername(String username) {
		log.debug("Finding user with username " + username);

		User returnedUser = new User();
		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ers_users WHERE ers_username = ?");
			stmt.setString(1, username);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return getUserFromResultSet(results);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire user from database.");
		}

		return returnedUser;
	}

	@Override
	public String getPasswordByUsername(String username) {
		log.debug("Finding password of user with username " + username);

		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn.prepareStatement("SELECT ers_password FROM ers_users WHERE ers_username = ?");
			stmt.setString(1, username);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return results.getString(1);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire user from database.");
		}

		return null;
	}

	@Override
	public User getUserById(int id) {
		log.debug("Finding user with user id " + id);

		User returnedUser = new User();
		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ers_users WHERE ers_users_id = ?");
			stmt.setInt(1, id);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return getUserFromResultSet(results);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire user from database.");
		}

		return returnedUser;
	}

	@Override
	public int createUser(String username, String password, String firstName, String lastName, int roleID,
			int statusID) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean userExists(String username, String email) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getFullNameById(int id) {
		log.debug("Finding full name with user id " + id);

		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ers_users WHERE ers_users_id = ?");
			stmt.setInt(1, id);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return getFullNameFromResultSet(results);
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire user from database.");
		}

		return null;
	}

	@Override
	public Map<Integer, String> getUserFullNames() {
		log.debug("Attempting to acquire map of all usernames... ");

		Map<Integer, String> usersMap = new HashMap<Integer, String>();
		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn.prepareStatement("SELECT ers_users_id, user_first_name, user_last_name FROM ers_users");
			ResultSet results = stmt.executeQuery();

			while(results.next()) {
				usersMap.put(results.getInt("ers_users_id"), results.getString("user_first_name") + " " + results.getString("user_last_name"));
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire user full names from database.");
		}

		return usersMap;
	}

}
