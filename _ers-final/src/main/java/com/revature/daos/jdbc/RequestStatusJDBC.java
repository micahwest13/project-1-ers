package com.revature.daos.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.revature.daos.RequestStatusDAO;
import com.revature.util.ConnectionUtil;

public class RequestStatusJDBC implements RequestStatusDAO {

	private static Logger log = Logger.getRootLogger();
	private static ConnectionUtil connUtil = ConnectionUtil.getConnectionUtil();

	@Override
	public String getStatusNameById(int id) {
		log.debug("Finding status of id " + id);

		try (Connection conn = connUtil.getConnection()) {

			PreparedStatement stmt = conn
					.prepareStatement("SELECT reimb_status FROM ers_reimbursement_status WHERE reimb_status_id = ?");
			stmt.setInt(1, id);
			ResultSet results = stmt.executeQuery();

			if (results.next()) {
				return results.getString("reimb_status");
			}

		} catch (SQLException e) {

			e.printStackTrace();
			log.warn("Failed to aquire status from database.");
		}

		return null;
	}

}
