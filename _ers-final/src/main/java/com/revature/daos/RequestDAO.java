package com.revature.daos;

import java.util.List;

import com.revature.beans.ReimbTicket;

public interface RequestDAO {
	
	/**
	 * @param id
	 * @return Returns a list of Reimbursement Requests from the given User ID.
	 */
	public List<ReimbTicket> getRequestsByUserId(int id);
	
	/**
	 * @param status code
	 * @return Returns a list of Reimbursement Requests from the given status code.
	 */
	public List<ReimbTicket> getRequestsByStatus(int statusCode);
	
	/**
	 * @param req
	 * @return Returns whether the creation of the request was successful.
	 */
	public int createRequest(ReimbTicket req);
	
	/**
	 * @return Returns a list of all reimbursements requests.
	 */
	public List<ReimbTicket> getAllRequests();
	
	/**
	 * @param id
	 * @param status
	 */
	public void updateStatus(int id, int userId, int status);
}
