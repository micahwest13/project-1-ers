package com.revature.daos;

public interface RequestStatusDAO {
	
	/**
	 * @param id
	 * @return Returns the name of the status with the given ID.
	 */
	public String getStatusNameById(int id);
}
