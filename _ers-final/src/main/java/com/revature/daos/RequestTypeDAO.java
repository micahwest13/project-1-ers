package com.revature.daos;

public interface RequestTypeDAO {
	
	/**
	 * @param id
	 * @return Returns the reimbursement type name from the given ID.
	 */
	public String getTypeNameById(int id);
}
