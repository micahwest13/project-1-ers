package com.revature.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.revature.beans.ClientTicket;
import com.revature.beans.ReimbTicket;
import com.revature.beans.User;
import com.revature.daos.RequestDAO;
import com.revature.daos.jdbc.RequestJDBC;
import com.revature.exceptions.ErsUnauthorizedException;
import com.revature.services.ErsRequestor;
import com.revature.services.JSONParser;

public class ManagerReimbRequestController implements DelegateController {
	
	private static Logger log = Logger.getRootLogger();
	private RequestDAO daoRequest = new RequestJDBC();
	private JSONParser jsonp = new JSONParser();

	@Override
	public void delegatePost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void delegateGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.info("Get All Reimbursements Received.");
		User currentUser = (User) request.getSession().getAttribute("user");
		
		if(currentUser == null || currentUser.getRoleID() != 2) {
			throw new ErsUnauthorizedException();
		} else {
			List<ReimbTicket> tickets = new ArrayList<ReimbTicket>();
			tickets = daoRequest.getAllRequests();
			
			List<ClientTicket> outTickets = new ErsRequestor().convertToClientTickets(tickets);
			
			String json = jsonp.JSONStringify(outTickets);
			response.getWriter().write(json);
		}
	}
	
	public void delegatePut(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.info("Put reimbursement received.");
		User currentUser = (User) request.getSession().getAttribute("user");
		
		if(currentUser == null || currentUser.getRoleID() != 2) {
			throw new ErsUnauthorizedException();
		} else {
			ErsRequestor requestor = new ErsRequestor();
			requestor.updateTicketStatus(request);
			
			delegateGet(request, response);
		}
	}
}
