package com.revature.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.revature.beans.LoginCredentials;
import com.revature.beans.User;
import com.revature.exceptions.ErsHttpException;
import com.revature.services.Authenticator;
import com.revature.services.ErsRequestor;
import com.revature.services.JSONParser;

public class LoginController implements DelegateController {
	private Logger log = Logger.getRootLogger();
	private JSONParser jsonp = new JSONParser();

	@Override
	public void delegateGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.info("In LoginController delegateGet");
		User currentUser = (User) request.getSession().getAttribute("user");
		String uri = request.getRequestURI().toLowerCase();
		String destination = uri.substring(request.getContextPath().length() + 1, uri.length());

		if (destination.equals("login")) {
			getLogin(request, response);
		}

		else if (destination.equals("logout")) {
			if (currentUser == null) {
				log.info("No user logged in.");
			} else {
				log.info("Logging out " + currentUser.getUserName() + "...");
				request.getSession().setAttribute("user", null);
			}
			response.sendRedirect("./home");
		}
	}

	private void getLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User currentUser = (User) request.getSession().getAttribute("user");

		log.debug("currentUser: " + currentUser);
		if (currentUser != null) {
			try {
				ObjectMapper map = new ObjectMapper();
				ObjectWriter writer = map.writer().withDefaultPrettyPrinter();
				String json = writer.writeValueAsString(currentUser);
				response.getWriter().write(json);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			log.info("Sending empty response to client.");
			String json = jsonp.JSONStringify(null);
			response.getWriter().write(json);
		}
	}

	@Override
	public void delegatePost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		User currentUser = (User) request.getSession().getAttribute("user");

		if (currentUser == null) {
			log.info("New user attempting to login on session " + request.getSession().getId() + "...");

			String json = request.getReader().lines().reduce((acc, cur) -> acc + cur).get();
			LoginCredentials login = jsonp.JSONParse(json, LoginCredentials.class);

			currentUser = handleLogin(login);
			request.getSession().setAttribute("user", currentUser);

			if (currentUser == null) {
				response.getWriter().write("null");
				log.info("Login failed. Returning to Front Controller.");
				return;
			}
			
			ErsRequestor requestor = new ErsRequestor();
			currentUser.setClientRequests(requestor.convertToClientTickets(currentUser.getRequests()));
			
			response.getWriter().write(jsonp.JSONStringify(currentUser));
			log.info("User login attempt complete. Returning to Front Controller.");
		}

		else {
			log.info("Current logged in user: " + currentUser.toString());
		}
	}

	private User handleLogin(LoginCredentials login) throws ErsHttpException {
		log.info("Creds received: " + login);

		String username = login.getUsername();
		String password = login.getPassword();
		Authenticator auth = new Authenticator();
		User currentUser = auth.attemptLogin(username, password);

		if (currentUser != null) {
			log.info("Login successful, acquiring user data for " + username + "...");
			log.info("User " + currentUser.getFirstName() + " " + currentUser.getLastName() + " retrieved.");
			log.info("Saving user to current session.");
			return currentUser;
		} else {
			log.info("Login failed. Setting session user to null.");
			return null;
		}
	}
}
