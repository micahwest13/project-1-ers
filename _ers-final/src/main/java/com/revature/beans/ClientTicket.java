package com.revature.beans;

public class ClientTicket {

	private int id;
	private double amount;
	private String submissionDate;
	private String resolutionDate;
	private String description;
	private String author;
	private String resolver;
	private String status;
	private String type;

	public ClientTicket() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ClientTicket(ReimbTicket ticket) {
		super();
		this.id = ticket.getId();
		this.amount = ticket.getAmount();
		this.submissionDate = ticket.getSubmissionDate();
		this.resolutionDate = ticket.getResolutionDate();
		this.description = ticket.getDescription();
		this.author = null;
		this.resolver = null;
		this.status = null;
		this.type = null;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getResolutionDate() {
		return resolutionDate;
	}

	public void setResolutionDate(String resolutionDate) {
		this.resolutionDate = resolutionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getResolver() {
		return resolver;
	}

	public void setResolver(String resolver) {
		this.resolver = resolver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setTypeId(String type) {
		this.type = type;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ClientTicket [id=" + id + ", amount=" + amount + ", submissionDate=" + submissionDate + ", resolutionDate="
				+ resolutionDate + ", description=" + description + ", author=" + author + ", resolver=" + resolver
				+ ", statusId=" + status + ", typeId=" + type + "]";
	}

}
